# Air Quality Indexing


## Description
It's a simple PWA to retrieve Air Quality Index (AQI) of the cities around the world. 
User can fetch the AQI of the desire city by providing the city name.

### Installation

1. Install Node by running the following command.

```bash
nvm install node <version>
```
For Example :
```bash
nvm install node 6.11.2
```

2. Clone project repository

```bash
git@gitlab.com:maythinkhiang/air-indexing.git
```

3. Go to project directory and run following commands.

```bash
npm install
npm start
```
