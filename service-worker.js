
var CACHE_NAME = 'static-cache';

var urlsToCache = [
    '.',
    'index.html',
    'styles/main.css',
    'styles/bootstrap/bootstrap.min.css',
    'styles/fontawesome-free/all.min.css',
    'styles/simple-line-icons/simple-line-icons.css',
    'styles/page.min.css',
    'js/jquery.min.js',
    'js/main.js',
    'js/bootstrap_js/bootstrap.bundle.min.js',
    'img/aqi-english_orig.jpg',
    'img/bg-masthead.jpg',
    'styles/webfonts/fa-brands-400.woff2'
];

self.addEventListener('install', function(event) {
    self.skipWaiting();
    console.log('Service worker activating...');
    event.waitUntil(
        caches.open(CACHE_NAME)
            .then(function(cache) {
                return cache.addAll(urlsToCache);
            })
    );
});

self.addEventListener('activate', function(event) {
    console.log('Service worker activating...');
});


// I'm a new service worker

self.addEventListener('fetch', event => {
    console.log('Fetch intercepted for:', event.request.url);
    event.respondWith(caches.match(event.request)
        .then(cachedResponse => {
            if (cachedResponse) {
                return cachedResponse;
            }
            return fetch(event.request);
        })
    );
});
