const apiKey = '3f527c3359d695f1104d910281570cd2f329e262';


document.addEventListener("DOMContentLoaded", () => {
    getIPAddress();
});

function getIPAddress() {
    fetch("https://jsonip.com")
        .then((resp) => resp.json())
        .then(function (data) {
            console.log(data);
            getCityName(data.ip);
        })
        .catch(logError);

}

function getCityName(ip) {
    fetch("http://api.ipstack.com/" + ip + "?access_key=56760035a686e7f457fcb93c9d3ee4c0&fields=city")
        .then((resp) => resp.json())
        .then(function (data) {
            console.log(data);
            // city = data.ip.city;
            fetchAirIndexApi(data.city);
        })
        .catch(logError);
}

function fetchAirIndexApi(city) {

    return fetch("http://api.waqi.info/feed/" + city + "/?token=" + apiKey)
        .then((resp) => resp.json())
        .then(function (result) {
            console.log(result.data);
            if(result.data == "Unknown station"){
                document.getElementById('aqi-info-card').style.display = "none";
                document.getElementById('error-message').style.display = "block";
                stationNotfound(result.data);
            }else{
                document.getElementById('aqi-info-card').style.display = "block";
                document.getElementById('error-message').style.display = "none";
                showResult(result.data, city)
            }
        })
        .catch(logError);
}

function fetchApi() {
    var city = document.getElementById('input_city').value;
    if (city) {
        fetchAirIndexApi(city)
    }
}

function showResult(data, city) {
    document.getElementById('city_name').innerHTML = "Air Quality Index (AQI) of <br>" + data.city.name + " :";
    document.getElementById('aqi_amount').innerHTML = data.aqi;
    if (data.aqi <= 50) {
        document.getElementById('aqi_label').innerHTML = "Good";
        document.getElementById('aqi-color').style.backgroundColor = "#009966";
    }else if (data.aqi <= 100 && data.aqi >= 51) {
        document.getElementById('aqi_label').innerHTML = "Moderate";
        document.getElementById('aqi-color').style.backgroundColor = "#ffde33";
    }else if (data.aqi <= 150 && data.aqi >= 101) {
        document.getElementById('aqi_label').innerHTML = "Unhealthy for Sensitive Group";
        document.getElementById('aqi-color').style.backgroundColor = "#ff9933";
    }else if (data.aqi <= 200 && data.aqi >= 151){
        document.getElementById('aqi_label').innerHTML = "Unhealthy";
        document.getElementById('aqi-color').style.backgroundColor = "#cc0033";
    }else if(data.aqi <= 300 && data.aqi >= 201){
        document.getElementById('aqi_label').innerHTML = "Very Unhealthy";
        document.getElementById('aqi-color').style.backgroundColor = "#660099";
    }else{
        document.getElementById('aqi_label').innerHTML = "Hazardous";
        document.getElementById('aqi-color').style.backgroundColor = "#7e0023";
    };
    document.getElementById('aqi_time').innerHTML = "Updated on : " + data.time.s;
}


function logError(error) {
    console.log('Looks like there was a problem:', error);
    document.getElementById('aqi-info-card').style.display = "none";
    document.getElementById('error-message').innerHTML = "Looks like there was network problem";
}

function stationNotfound(message) {
    document.getElementById('error-message').innerHTML = message;
}
